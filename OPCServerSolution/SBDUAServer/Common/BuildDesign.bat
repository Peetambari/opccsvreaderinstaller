@echo off
setlocal

SET PATH=%PATH%;..\..\Bin\Debug;..\..\Bin

REM These lines will build the sample design if the batch file is run from the project directory

echo Building FileSystem
Opc.Ua.ModelCompiler.exe -d2 ".\FileSystem\FileSystemDesign.xml" -cg ".\FileSystem\FileSystemDesign.csv" -o ".\FileSystem" 
echo Success!