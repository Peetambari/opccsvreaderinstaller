/* ========================================================================
 * Copyright (c) 2005-2016 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using Opc.Ua;

namespace FileSystem
{
    #region Object Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Objects
    {
        /// <summary>
        /// The identifier for the Files Object.
        /// </summary>
        public const uint Files = 15002;
    }
    #endregion

    #region ObjectType Identifiers
    /// <summary>
    /// A class that declares constants for all ObjectTypes in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectTypes
    {
        /// <summary>
        /// The identifier for the GenericCsvType ObjectType.
        /// </summary>
        public const uint GenericCsvType = 15040;
    }
    #endregion

    #region Variable Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Variables
    {
        /// <summary>
        /// The identifier for the GenericCsvType_FilePath Variable.
        /// </summary>
        public const uint GenericCsvType_FilePath = 15048;

        /// <summary>
        /// The identifier for the GenericCsvType_LastUpdateTime Variable.
        /// </summary>
        public const uint GenericCsvType_LastUpdateTime = 15049;

        /// <summary>
        /// The identifier for the GenericCsvType_IsConnected Variable.
        /// </summary>
        public const uint GenericCsvType_IsConnected = 15050;

        /// <summary>
        /// The identifier for the GenericCsvType_CurrentRecord Variable.
        /// </summary>
        public const uint GenericCsvType_CurrentRecord = 15041;

        /// <summary>
        /// The identifier for the GenericCsvType_Move Variable.
        /// </summary>
        public const uint GenericCsvType_Move = 15042;

        /// <summary>
        /// The identifier for the GenericCsvType_MoveFirst Variable.
        /// </summary>
        public const uint GenericCsvType_MoveFirst = 15043;

        /// <summary>
        /// The identifier for the GenericCsvType_MoveLast Variable.
        /// </summary>
        public const uint GenericCsvType_MoveLast = 15044;

        /// <summary>
        /// The identifier for the GenericCsvType_MoveNext Variable.
        /// </summary>
        public const uint GenericCsvType_MoveNext = 15045;

        /// <summary>
        /// The identifier for the GenericCsvType_MovePrev Variable.
        /// </summary>
        public const uint GenericCsvType_MovePrev = 15046;

        /// <summary>
        /// The identifier for the GenericCsvType_RecordCount Variable.
        /// </summary>
        public const uint GenericCsvType_RecordCount = 15047;

        /// <summary>
        /// The identifier for the GenericCsvType_ReadRecord Variable.
        /// </summary>
        public const uint GenericCsvType_ReadRecord = 15051;
    }
    #endregion

    #region Object Node Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectIds
    {
        /// <summary>
        /// The identifier for the Files Object.
        /// </summary>
        public static readonly ExpandedNodeId Files = new ExpandedNodeId(FileSystem.Objects.Files, FileSystem.Namespaces.FileSystem);
    }
    #endregion

    #region ObjectType Node Identifiers
    /// <summary>
    /// A class that declares constants for all ObjectTypes in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectTypeIds
    {
        /// <summary>
        /// The identifier for the GenericCsvType ObjectType.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType = new ExpandedNodeId(FileSystem.ObjectTypes.GenericCsvType, FileSystem.Namespaces.FileSystem);
    }
    #endregion

    #region Variable Node Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class VariableIds
    {
        /// <summary>
        /// The identifier for the GenericCsvType_FilePath Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_FilePath = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_FilePath, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_LastUpdateTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_LastUpdateTime = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_LastUpdateTime, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_IsConnected Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_IsConnected = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_IsConnected, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_CurrentRecord Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_CurrentRecord = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_CurrentRecord, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_Move Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_Move = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_Move, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_MoveFirst Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_MoveFirst = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_MoveFirst, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_MoveLast Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_MoveLast = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_MoveLast, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_MoveNext Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_MoveNext = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_MoveNext, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_MovePrev Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_MovePrev = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_MovePrev, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_RecordCount Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_RecordCount = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_RecordCount, FileSystem.Namespaces.FileSystem);

        /// <summary>
        /// The identifier for the GenericCsvType_ReadRecord Variable.
        /// </summary>
        public static readonly ExpandedNodeId GenericCsvType_ReadRecord = new ExpandedNodeId(FileSystem.Variables.GenericCsvType_ReadRecord, FileSystem.Namespaces.FileSystem);
    }
    #endregion

    #region BrowseName Declarations
    /// <summary>
    /// Declares all of the BrowseNames used in the Model Design.
    /// </summary>
    public static partial class BrowseNames
    {
        /// <summary>
        /// The BrowseName for the CurrentRecord component.
        /// </summary>
        public const string CurrentRecord = "CurrentRecord";

        /// <summary>
        /// The BrowseName for the FilePath component.
        /// </summary>
        public const string FilePath = "FilePath";

        /// <summary>
        /// The BrowseName for the Files component.
        /// </summary>
        public const string Files = "Files";

        /// <summary>
        /// The BrowseName for the GenericCsvType component.
        /// </summary>
        public const string GenericCsvType = "GenericCsvType";

        /// <summary>
        /// The BrowseName for the IsConnected component.
        /// </summary>
        public const string IsConnected = "IsConnected";

        /// <summary>
        /// The BrowseName for the LastUpdateTime component.
        /// </summary>
        public const string LastUpdateTime = "LastUpdateTime";

        /// <summary>
        /// The BrowseName for the Move component.
        /// </summary>
        public const string Move = "Move";

        /// <summary>
        /// The BrowseName for the MoveFirst component.
        /// </summary>
        public const string MoveFirst = "MoveFirst";

        /// <summary>
        /// The BrowseName for the MoveLast component.
        /// </summary>
        public const string MoveLast = "MoveLast";

        /// <summary>
        /// The BrowseName for the MoveNext component.
        /// </summary>
        public const string MoveNext = "MoveNext";

        /// <summary>
        /// The BrowseName for the MovePrev component.
        /// </summary>
        public const string MovePrev = "MovePrev";

        /// <summary>
        /// The BrowseName for the ReadRecord component.
        /// </summary>
        public const string ReadRecord = "ReadRecord";

        /// <summary>
        /// The BrowseName for the RecordCount component.
        /// </summary>
        public const string RecordCount = "RecordCount";
    }
    #endregion

    #region Namespace Declarations
    /// <summary>
    /// Defines constants for all namespaces referenced by the model design.
    /// </summary>
    public static partial class Namespaces
    {
        /// <summary>
        /// The URI for the OpcUa namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUa = "http://opcfoundation.org/UA/";

        /// <summary>
        /// The URI for the OpcUaXsd namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUaXsd = "http://opcfoundation.org/UA/2008/02/Types.xsd";

        /// <summary>
        /// The URI for the FileSystem namespace (.NET code namespace is 'FileSystem').
        /// </summary>
        public const string FileSystem = "http://opcfoundation.org/UA/FileSystem/";
    }
    #endregion

    #region GenericCsvState Class
    #if (!OPCUA_EXCLUDE_GenericCsvState)
    /// <summary>
    /// Stores an instance of the GenericCsvType ObjectType.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public partial class GenericCsvState : BaseObjectState
    {
        #region Constructors
        /// <summary>
        /// Initializes the type with its default attribute values.
        /// </summary>
        public GenericCsvState(NodeState parent) : base(parent)
        {
        }

        /// <summary>
        /// Returns the id of the default type definition node for the instance.
        /// </summary>
        protected override NodeId GetDefaultTypeDefinitionId(NamespaceTable namespaceUris)
        {
            return Opc.Ua.NodeId.Create(FileSystem.ObjectTypes.GenericCsvType, FileSystem.Namespaces.FileSystem, namespaceUris);
        }

        #if (!OPCUA_EXCLUDE_InitializationStrings)
        /// <summary>
        /// Initializes the instance.
        /// </summary>
        protected override void Initialize(ISystemContext context)
        {
            Initialize(context, InitializationString);
            InitializeOptionalChildren(context);
        }

        /// <summary>
        /// Initializes the instance with a node.
        /// </summary>
        protected override void Initialize(ISystemContext context, NodeState source)
        {
            InitializeOptionalChildren(context);
            base.Initialize(context, source);
        }

        /// <summary>
        /// Initializes the any option children defined for the instance.
        /// </summary>
        protected override void InitializeOptionalChildren(ISystemContext context)
        {
            base.InitializeOptionalChildren(context);
        }

        #region Initialization String
        private const string InitializationString =
           "AQAAACcAAABodHRwOi8vb3BjZm91bmRhdGlvbi5vcmcvVUEvRmlsZVN5c3RlbS//////BGCAAAEAAAAB" +
           "ABYAAABHZW5lcmljQ3N2VHlwZUluc3RhbmNlAQHAOgEBwDr/////CwAAABVgiQoCAAAAAQAIAAAARmls" +
           "ZVBhdGgBAcg6AC4ARMg6AAAADP////8BAf////8AAAAAFWCJCgIAAAABAA4AAABMYXN0VXBkYXRlVGlt" +
           "ZQEByToALgBEyToAAAAN/////wEB/////wAAAAAVYIkKAgAAAAEACwAAAElzQ29ubmVjdGVkAQHKOgAu" +
           "AETKOgAAAAz/////AQH/////AAAAABVgiQoCAAAAAQANAAAAQ3VycmVudFJlY29yZAEBwToALgBEwToA" +
           "AAAG/////wEB/////wAAAAAVYIkKAgAAAAEABAAAAE1vdmUBAcI6AC4ARMI6AAAABv////8DA/////8A" +
           "AAAAFWCJCgIAAAABAAkAAABNb3ZlRmlyc3QBAcM6AC4ARMM6AAAAAf////8DA/////8AAAAAFWCJCgIA" +
           "AAABAAgAAABNb3ZlTGFzdAEBxDoALgBExDoAAAAB/////wMD/////wAAAAAVYIkKAgAAAAEACAAAAE1v" +
           "dmVOZXh0AQHFOgAuAETFOgAAAAH/////AwP/////AAAAABVgiQoCAAAAAQAIAAAATW92ZVByZXYBAcY6" +
           "AC4ARMY6AAAAAf////8DA/////8AAAAAFWCJCgIAAAABAAsAAABSZWNvcmRDb3VudAEBxzoALgBExzoA" +
           "AAAG/////wEB/////wAAAAAVYIkKAgAAAAEACgAAAFJlYWRSZWNvcmQBAcs6AC4ARMs6AAAABv////8B" +
           "Af////8AAAAA";
        #endregion
        #endif
        #endregion

        #region Public Properties
        /// <summary>
        /// A description for the FilePath Property.
        /// </summary>
        public PropertyState<string> FilePath
        {
            get
            {
                return m_filePath;
            }

            set
            {
                if (!Object.ReferenceEquals(m_filePath, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_filePath = value;
            }
        }

        /// <summary>
        /// A description for the LastUpdateTime Property.
        /// </summary>
        public PropertyState<DateTime> LastUpdateTime
        {
            get
            {
                return m_lastUpdateTime;
            }

            set
            {
                if (!Object.ReferenceEquals(m_lastUpdateTime, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_lastUpdateTime = value;
            }
        }

        /// <summary>
        /// A description for the IsConnected Property.
        /// </summary>
        public PropertyState<string> IsConnected
        {
            get
            {
                return m_isConnected;
            }

            set
            {
                if (!Object.ReferenceEquals(m_isConnected, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_isConnected = value;
            }
        }

        /// <summary>
        /// A description for the CurrentRecord Property.
        /// </summary>
        public PropertyState<int> CurrentRecord
        {
            get
            {
                return m_currentRecord;
            }

            set
            {
                if (!Object.ReferenceEquals(m_currentRecord, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_currentRecord = value;
            }
        }

        /// <summary>
        /// A description for the Move Property.
        /// </summary>
        public PropertyState<int> Move
        {
            get
            {
                return m_move;
            }

            set
            {
                if (!Object.ReferenceEquals(m_move, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_move = value;
            }
        }

        /// <summary>
        /// A description for the MoveFirst Property.
        /// </summary>
        public PropertyState<bool> MoveFirst
        {
            get
            {
                return m_moveFirst;
            }

            set
            {
                if (!Object.ReferenceEquals(m_moveFirst, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_moveFirst = value;
            }
        }

        /// <summary>
        /// A description for the MoveLast Property.
        /// </summary>
        public PropertyState<bool> MoveLast
        {
            get
            {
                return m_moveLast;
            }

            set
            {
                if (!Object.ReferenceEquals(m_moveLast, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_moveLast = value;
            }
        }

        /// <summary>
        /// A description for the MoveNext Property.
        /// </summary>
        public PropertyState<bool> MoveNext
        {
            get
            {
                return m_moveNext;
            }

            set
            {
                if (!Object.ReferenceEquals(m_moveNext, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_moveNext = value;
            }
        }

        /// <summary>
        /// A description for the MovePrev Property.
        /// </summary>
        public PropertyState<bool> MovePrev
        {
            get
            {
                return m_movePrev;
            }

            set
            {
                if (!Object.ReferenceEquals(m_movePrev, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_movePrev = value;
            }
        }

        /// <summary>
        /// A description for the RecordCount Property.
        /// </summary>
        public PropertyState<int> RecordCount
        {
            get
            {
                return m_recordCount;
            }

            set
            {
                if (!Object.ReferenceEquals(m_recordCount, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_recordCount = value;
            }
        }

        /// <summary>
        /// A description for the ReadRecord Property.
        /// </summary>
        public PropertyState<int> ReadRecord
        {
            get
            {
                return m_readRecord;
            }

            set
            {
                if (!Object.ReferenceEquals(m_readRecord, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_readRecord = value;
            }
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Populates a list with the children that belong to the node.
        /// </summary>
        /// <param name="context">The context for the system being accessed.</param>
        /// <param name="children">The list of children to populate.</param>
        public override void GetChildren(
            ISystemContext context,
            IList<BaseInstanceState> children)
        {
            if (m_filePath != null)
            {
                children.Add(m_filePath);
            }

            if (m_lastUpdateTime != null)
            {
                children.Add(m_lastUpdateTime);
            }

            if (m_isConnected != null)
            {
                children.Add(m_isConnected);
            }

            if (m_currentRecord != null)
            {
                children.Add(m_currentRecord);
            }

            if (m_move != null)
            {
                children.Add(m_move);
            }

            if (m_moveFirst != null)
            {
                children.Add(m_moveFirst);
            }

            if (m_moveLast != null)
            {
                children.Add(m_moveLast);
            }

            if (m_moveNext != null)
            {
                children.Add(m_moveNext);
            }

            if (m_movePrev != null)
            {
                children.Add(m_movePrev);
            }

            if (m_recordCount != null)
            {
                children.Add(m_recordCount);
            }

            if (m_readRecord != null)
            {
                children.Add(m_readRecord);
            }

            base.GetChildren(context, children);
        }

        /// <summary>
        /// Finds the child with the specified browse name.
        /// </summary>
        protected override BaseInstanceState FindChild(
            ISystemContext context,
            QualifiedName browseName,
            bool createOrReplace,
            BaseInstanceState replacement)
        {
            if (QualifiedName.IsNull(browseName))
            {
                return null;
            }

            BaseInstanceState instance = null;

            switch (browseName.Name)
            {
                case FileSystem.BrowseNames.FilePath:
                {
                    if (createOrReplace)
                    {
                        if (FilePath == null)
                        {
                            if (replacement == null)
                            {
                                FilePath = new PropertyState<string>(this);
                            }
                            else
                            {
                                FilePath = (PropertyState<string>)replacement;
                            }
                        }
                    }

                    instance = FilePath;
                    break;
                }

                case FileSystem.BrowseNames.LastUpdateTime:
                {
                    if (createOrReplace)
                    {
                        if (LastUpdateTime == null)
                        {
                            if (replacement == null)
                            {
                                LastUpdateTime = new PropertyState<DateTime>(this);
                            }
                            else
                            {
                                LastUpdateTime = (PropertyState<DateTime>)replacement;
                            }
                        }
                    }

                    instance = LastUpdateTime;
                    break;
                }

                case FileSystem.BrowseNames.IsConnected:
                {
                    if (createOrReplace)
                    {
                        if (IsConnected == null)
                        {
                            if (replacement == null)
                            {
                                IsConnected = new PropertyState<string>(this);
                            }
                            else
                            {
                                IsConnected = (PropertyState<string>)replacement;
                            }
                        }
                    }

                    instance = IsConnected;
                    break;
                }

                case FileSystem.BrowseNames.CurrentRecord:
                {
                    if (createOrReplace)
                    {
                        if (CurrentRecord == null)
                        {
                            if (replacement == null)
                            {
                                CurrentRecord = new PropertyState<int>(this);
                            }
                            else
                            {
                                CurrentRecord = (PropertyState<int>)replacement;
                            }
                        }
                    }

                    instance = CurrentRecord;
                    break;
                }

                case FileSystem.BrowseNames.Move:
                {
                    if (createOrReplace)
                    {
                        if (Move == null)
                        {
                            if (replacement == null)
                            {
                                Move = new PropertyState<int>(this);
                            }
                            else
                            {
                                Move = (PropertyState<int>)replacement;
                            }
                        }
                    }

                    instance = Move;
                    break;
                }

                case FileSystem.BrowseNames.MoveFirst:
                {
                    if (createOrReplace)
                    {
                        if (MoveFirst == null)
                        {
                            if (replacement == null)
                            {
                                MoveFirst = new PropertyState<bool>(this);
                            }
                            else
                            {
                                MoveFirst = (PropertyState<bool>)replacement;
                            }
                        }
                    }

                    instance = MoveFirst;
                    break;
                }

                case FileSystem.BrowseNames.MoveLast:
                {
                    if (createOrReplace)
                    {
                        if (MoveLast == null)
                        {
                            if (replacement == null)
                            {
                                MoveLast = new PropertyState<bool>(this);
                            }
                            else
                            {
                                MoveLast = (PropertyState<bool>)replacement;
                            }
                        }
                    }

                    instance = MoveLast;
                    break;
                }

                case FileSystem.BrowseNames.MoveNext:
                {
                    if (createOrReplace)
                    {
                        if (MoveNext == null)
                        {
                            if (replacement == null)
                            {
                                MoveNext = new PropertyState<bool>(this);
                            }
                            else
                            {
                                MoveNext = (PropertyState<bool>)replacement;
                            }
                        }
                    }

                    instance = MoveNext;
                    break;
                }

                case FileSystem.BrowseNames.MovePrev:
                {
                    if (createOrReplace)
                    {
                        if (MovePrev == null)
                        {
                            if (replacement == null)
                            {
                                MovePrev = new PropertyState<bool>(this);
                            }
                            else
                            {
                                MovePrev = (PropertyState<bool>)replacement;
                            }
                        }
                    }

                    instance = MovePrev;
                    break;
                }

                case FileSystem.BrowseNames.RecordCount:
                {
                    if (createOrReplace)
                    {
                        if (RecordCount == null)
                        {
                            if (replacement == null)
                            {
                                RecordCount = new PropertyState<int>(this);
                            }
                            else
                            {
                                RecordCount = (PropertyState<int>)replacement;
                            }
                        }
                    }

                    instance = RecordCount;
                    break;
                }

                case FileSystem.BrowseNames.ReadRecord:
                {
                    if (createOrReplace)
                    {
                        if (ReadRecord == null)
                        {
                            if (replacement == null)
                            {
                                ReadRecord = new PropertyState<int>(this);
                            }
                            else
                            {
                                ReadRecord = (PropertyState<int>)replacement;
                            }
                        }
                    }

                    instance = ReadRecord;
                    break;
                }
            }

            if (instance != null)
            {
                return instance;
            }

            return base.FindChild(context, browseName, createOrReplace, replacement);
        }
        #endregion

        #region Private Fields
        private PropertyState<string> m_filePath;
        private PropertyState<DateTime> m_lastUpdateTime;
        private PropertyState<string> m_isConnected;
        private PropertyState<int> m_currentRecord;
        private PropertyState<int> m_move;
        private PropertyState<bool> m_moveFirst;
        private PropertyState<bool> m_moveLast;
        private PropertyState<bool> m_moveNext;
        private PropertyState<bool> m_movePrev;
        private PropertyState<int> m_recordCount;
        private PropertyState<int> m_readRecord;
        #endregion
    }
    #endif
    #endregion
}