﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FileSystem
{
    /// <summary>
    /// Stores the configuration the test node manager
    /// </summary>
    [DataContract(Namespace = Namespaces.FileSystem)]
    public class FileSystemConfiguration
    {
        #region Constructors
        /// <summary>
        /// The default constructor.
        /// </summary>
        public FileSystemConfiguration()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes the object during deserialization.
        /// </summary>
        [OnDeserializing()]
        private void Initialize(StreamingContext context)
        {
            Initialize();
        }

        /// <summary>
        /// Sets private members to default values.
        /// </summary>
        private void Initialize()
        {
            m_files = null;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The buffers exposed by the memory 
        /// </summary>
        [DataMember(Order = 1)]
        public FileInstanceCollection Files
        {
            get { return m_files; }
            set { m_files = value; }
        }
        #endregion

        #region Private Members
        private FileInstanceCollection m_files;
        #endregion
    }

    /// <summary>
    /// Stores the configuration for a memory buffer instance.
    /// </summary>
    [DataContract(Namespace = Namespaces.FileSystem)]
    public class FileInstance
    {
        #region Constructors
        /// <summary>
        /// The default constructor.
        /// </summary>
        public FileInstance()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes the object during deserialization.
        /// </summary>
        [OnDeserializing()]
        private void Initialize(StreamingContext context)
        {
            Initialize();
        }

        /// <summary>
        /// Sets private members to default values.
        /// </summary>
        private void Initialize()
        {
            m_fileType = null;
            m_filePath = null;
        }
        #endregion

        #region Public Properties
        [DataMember(Order = 1)]
        public string FileType
        {
            get { return m_fileType; }
            set { m_fileType = value; }
        }

        [DataMember(Order = 2)]
        public string FilePath
        {
            get { return m_filePath; }
            set { m_filePath = value; }
        }
        #endregion

        #region Private Members
        private string m_fileType;
        private string m_filePath;
        #endregion
    }

    #region FileInstanceCollection Class
    /// <summary>
    /// A collection of FileInstances.
    /// </summary>
    [CollectionDataContract(Name = "ListOfFileInstance", Namespace = Namespaces.FileSystem, ItemName = "FileInstance")]
    public partial class FileInstanceCollection : List<FileInstance>
    {
    }
    #endregion
}
