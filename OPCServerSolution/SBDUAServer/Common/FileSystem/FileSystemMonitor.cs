/* ========================================================================
 * Copyright (c) 2005-2017 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Globalization;
using Opc.Ua;
using Opc.Ua.Server;
using System.Configuration;

namespace FileSystem
{
    /// <summary>
    /// Provides access to a file system that contains configuration files for controllers.
    /// </summary>
    public class FileSystemMonitor : IDisposable
    {
        #region Constructors
        /// <summary>
        /// Initializes the object with the root directory and the namespace index assigned to the node manager.
        /// </summary>
        public FileSystemMonitor(
            object dataLock)
        {
            m_dataLock = dataLock;
            m_monitoredStates = new List<IMonitoredState>();
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Frees any unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// An overrideable version of the Dispose.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Timer timer = m_timer;
                Utils.SilentDispose(timer);
                m_timer = null;

                Timer networktimer = n_timer;
                Utils.SilentDispose(networktimer);
                n_timer = null;
            }
        }
        #endregion

        #region Public Properties

        #endregion

        #region Public Methods

        public void AddMonitoredState(IMonitoredState state)
        {
            m_monitoredStates.Add(state);
        }

        /// <summary>
        /// Starts the monitoring timer.
        /// </summary>
        public void StartTimer()
        {
            int updateFileInterval = Convert.ToInt32(ConfigurationManager.AppSettings["UpdateFileInterval"].ToString()) ;
            if (m_timer == null)
            {
                m_timer = new Timer(OnUpdate, null, 5000, updateFileInterval);
            }
          
        }


        /// <summary>
        /// Starts the monitoring timer.
        /// </summary>
        public void StartTimerForNetworkStatus()
        {
            int UpdateNetworkStatus = Convert.ToInt32(ConfigurationManager.AppSettings["UpdateNetworkStatus"].ToString());
            if (n_timer == null)
            {
                n_timer = new Timer(OnUpdateNetwork, null, 300000, UpdateNetworkStatus);
            }
        }

        public void StopTimer()
        {
            if (m_timer == null)
            {
                m_timer.Dispose();
                m_timer = null;
            }
            if (n_timer == null)
            {
                n_timer.Dispose();
                n_timer = null;
            }
        }


        /// <summary>
        /// Periodically checks for changes.
        /// </summary>
        private void OnUpdate(object state)
        {
            try
            {
                lock (m_dataLock)
                {
                    // check if halted.
                    if (m_monitoredStates.Count == 0)
                    {
                        return;
                    }

                    for (int ii = 0; ii < m_monitoredStates.Count; ii++)
                    {
                        IMonitoredState monitoredState = m_monitoredStates[ii];
                        Thread threadOnUpdate = new Thread(new ThreadStart(monitoredState.CheckForChanges));

                         threadOnUpdate.Start();
                    }
                }
            }
            catch (Exception e)
            {
                Utils.Trace(e, "FileSystemMonitor: OnUpdate: Unexpected error updating monitored objects.");
            }
        }
        #endregion


        //Added By - Peetambari Added For - Network status Check for 5 min
        #region"OnUpdateNetwork"
        /// <summary>
        /// Periodically checks for changes for network status.
        /// </summary>
        private void OnUpdateNetwork(object state)
        {
            try
            {
                {
                    // check if halted.
                    if (m_monitoredStates.Count == 0)
                    {
                        return;
                    }

                    DateTime today = DateTime.Now;

                    Opc.Ua.Utils.Trace(" Network Status Timer Thread Started " + today);

                    for (int ii = 0; ii < m_monitoredStates.Count; ii++)
                    {
                        IMonitoredState monitoredState = m_monitoredStates[ii];

                        Thread thread = new Thread(new ThreadStart(monitoredState.CheckForNetworkStatusChanges));

                        //    System.Threading.Thread thread = new System.Threading.Thread(() => monitoredState.CheckForNetworkStatusChanges());
                        thread.Start();

                    }
                    Opc.Ua.Utils.Trace(" Network Status Timer Thread Ended " + today);
                }
            }
            catch (Exception e)
            {
                Utils.Trace(e, "FileSystemMonitor: OnUpdateNetwork : Unexpected error updating Network objects.");
            }
        }
        #endregion


        #region Private Fields
        private object m_dataLock;
        private List<IMonitoredState> m_monitoredStates;
        private Timer m_timer;
        private Timer n_timer;
        #endregion
    }

    public class Constants
    {
        public const string C007 = "C007";
    }
}
