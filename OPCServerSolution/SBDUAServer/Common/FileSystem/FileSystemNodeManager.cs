/* ========================================================================
 * Copyright (c) 2005-2017 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Threading;
using Opc.Ua;
using Opc.Ua.Sample;

namespace FileSystem
{
    /// <summary>
    /// A node manager for a variety of test data.
    /// </summary>
    public class FileSystemNodeManager : SampleNodeManager
    {
        #region Constructors
        /// <summary>
        /// Initializes the node manager.
        /// </summary>
        public FileSystemNodeManager(Opc.Ua.Server.IServerInternal server, ApplicationConfiguration configuration)
        :
            base(server)
        {
            List<string> namespaceUris = new List<string>();

            namespaceUris.Add(Namespaces.FileSystem);
            namespaceUris.Add(Namespaces.FileSystem + "Instance");

            NamespaceUris = namespaceUris;

            m_typeNamespaceIndex = Server.NamespaceUris.GetIndexOrAppend(namespaceUris[0]);
            m_namespaceIndex = Server.NamespaceUris.GetIndexOrAppend(namespaceUris[1]);
            m_lastUsedId = 0;

            m_cache = new NodeIdDictionary<NodeState>();

            // get the configuration for the node manager.
            m_configuration = configuration.ParseExtension<FileSystemConfiguration>();

            // use suitable defaults if no configuration exists.
            if (m_configuration == null)
            {
                m_configuration = new FileSystemConfiguration();
            }

            // create the system.
            m_system = new FileSystemMonitor(Lock);

            // update the default context.
            SystemContext.SystemHandle = m_system;
        }
        #endregion

        #region IDisposable Members
        /// <summary>
        /// An overrideable version of the Dispose.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Utils.SilentDispose(m_system);
                m_system = null;
            }

            base.Dispose(disposing);
        }
        #endregion

        #region INodeIdFactory Members
        /// <summary>
        /// Creates the NodeId for the specified node.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        /// <returns>The new NodeId.</returns>


        // -------Commented For - NodeId Issue. Commented By - Peetambari------------
        //public override NodeId New(ISystemContext context, NodeState node)
        //{
        //    uint id = Utils.IncrementIdentifier(ref m_lastUsedId);
        //    return new NodeId(id, m_namespaceIndex);
        //}


        public override NodeId New(ISystemContext context, NodeState node)
        {
            if (node.NodeClass == NodeClass.Object)
            {
                string name = node.SymbolicName;
                return new NodeId(name, m_namespaceIndex);
            }
            else if (node.NodeClass == NodeClass.Variable)
            {
                var parent = (node as BaseInstanceState).Parent;
                string name = parent != null ? parent.SymbolicName + "#" + node.SymbolicName : node.SymbolicName;
                return new NodeId(name, m_namespaceIndex);
            }

            uint id = Utils.IncrementIdentifier(ref m_lastUsedId);
            return new NodeId(id, m_namespaceIndex);
        }
        #endregion

        #region INodeManager Members
        /// <summary>
        /// Does any initialization required before the address space can be used.
        /// </summary>
        /// <remarks>
        /// The externalReferences is an out parameter that allows the node manager to link to nodes
        /// in other node managers. For example, the 'Objects' node is managed by the CoreNodeManager and
        /// should have a reference to the root folder node(s) exposed by this node manager.  
        /// </remarks>
        public override void CreateAddressSpace(IDictionary<NodeId, IList<IReference>> externalReferences)
        {
            lock (Lock)
            {
                base.CreateAddressSpace(externalReferences);

                // add the file system objects
                CreateNodeStatesFromFilePaths();

                // start monitoring
                m_system.StartTimer();
                m_system.StartTimerForNetworkStatus();
            }
        }

        /// <summary>
        /// Loads a node set from a file or resource and addes them to the set of predefined nodes.
        /// </summary>
        protected override NodeStateCollection LoadPredefinedNodes(ISystemContext context)
        {
            NodeStateCollection predefinedNodes = new NodeStateCollection();
            predefinedNodes.LoadFromBinaryResource(context, "Opc.Ua.Sample.FileSystem.FileSystem.PredefinedNodes.uanodes", null, true);
            return predefinedNodes;
        }

        /// <summary>
        /// Returns a unique handle for the node.
        /// </summary>
        /// <remarks>
        /// This must efficiently determine whether the node belongs to the node manager. If it does belong to 
        /// NodeManager it should return a handle that does not require the NodeId to be validated again when
        /// the handle is passed into other methods such as 'Read' or 'Write'.
        /// </remarks>
        protected override object GetManagerHandle(ISystemContext context, NodeId nodeId, IDictionary<NodeId, NodeState> cache)
        {
            lock (Lock)
            {
                if (!IsNodeIdInNamespace(nodeId))
                {
                    return null;
                }

                // check for cached node.
                NodeState node = null;

                if (m_cache.TryGetValue(nodeId, out node))
                {
                    return node;
                }

                // base class will look up predefined nodes. 
                return base.GetManagerHandle(context, nodeId, cache);
            }
        }

        private void CreateNodeStatesFromFilePaths()
        {
            try
            {
                int counter = 1;

                // loop through the configuration
                foreach (var file in m_configuration.Files)
                {
                    if (file.FilePath == null)
                    {
                        continue;
                    }

                    // create state
                    var csvState = new GenericCsvState(SystemContext, m_namespaceIndex, file);

                    //string name = file.FileType + String.Format("_{0:D2}", counter);
                    string name = file.FileType;

                    csvState.Create(
                        SystemContext,
                        null,
                        new QualifiedName(name, m_namespaceIndex),
                        null,
                        true);

                    // add references
                    NodeState files = (NodeState)FindPredefinedNode(ExpandedNodeId.ToNodeId(ObjectIds.Files, Server.NamespaceUris), typeof(NodeState));

                    files.AddReference(Opc.Ua.ReferenceTypeIds.Organizes, false, csvState.NodeId);
                    csvState.AddReference(Opc.Ua.ReferenceTypeIds.Organizes, true, files.NodeId);

                    // add to collection
                    m_system.AddMonitoredState(csvState);
                    AddPredefinedNode(SystemContext, csvState);

                    counter += 1;
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "FileSystemNodeManager: CreateNodeStatesFromFilePaths: Error in CreatingNode of csv file: {0}");
            }
        }

        /// <summary>
        /// Does any processing after a monitored item is created.
        /// </summary>
        protected override void OnCreateMonitoredItem(
            ISystemContext systemContext,
            MonitoredItemCreateRequest itemToCreate,
            MonitoredNode monitoredNode,
            DataChangeMonitoredItem monitoredItem)
        {
        }

        /// <summary>
        /// Does any processing after a monitored item is created.
        /// </summary>
        protected override void OnModifyMonitoredItem(
            ISystemContext systemContext,
            MonitoredItemModifyRequest itemToModify,
            MonitoredNode monitoredNode,
            DataChangeMonitoredItem monitoredItem,
            double previousSamplingInterval)
        {
            // nothing to do.
        }

        /// <summary>
        /// Does any processing after a monitored item is deleted.
        /// </summary>
        protected override void OnDeleteMonitoredItem(
            ISystemContext systemContext,
            MonitoredNode monitoredNode,
            DataChangeMonitoredItem monitoredItem)
        {
        }

        /// <summary>
        /// Does any processing after a monitored item is created.
        /// </summary>
        protected override void OnSetMonitoringMode(
            ISystemContext systemContext,
            MonitoredNode monitoredNode,
            DataChangeMonitoredItem monitoredItem,
            MonitoringMode previousMode,
            MonitoringMode currentMode)
        {
            // nothing to do.
        }
        #endregion

        #region Private Fields
        private FileSystemConfiguration m_configuration;
        private FileSystemMonitor m_system;
        private NodeIdDictionary<NodeState> m_cache;
        private long m_lastUsedId;
        private ushort m_namespaceIndex;
        private ushort m_typeNamespaceIndex;
        #endregion
    }
}
