﻿using Opc.Ua;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;

namespace FileSystem
{
    public partial class GenericCsvState : IMonitoredState
    {
        private DateTime m_lastUpdatedTime;
        private int m_currRec = 0;
        private int m_rowCount = 0;
        private string m_ConnStatus = "false";
        private ISystemContext m_context;
        private ushort m_namespaceIndex;
        private FileInstance m_fileInstance;
        private Dictionary<int, PropertyState> m_children;
        Dictionary<string, string> dict = new Dictionary<string, string>();
        Dictionary<string, int> currentRecordDict = new Dictionary<string, int>();


        public GenericCsvState(ISystemContext context, ushort namespaceIndex, FileInstance fileInstance) : base(null)
        {
            m_context = context;
            m_namespaceIndex = namespaceIndex;
            m_fileInstance = fileInstance;
        }

        #region "CheckForChanges"
        /// <summary>
        /// Checks File updated status every 10 seconds. If updated it will continue to read data from Csv file.
        /// </summary>
        public void CheckForChanges()
        {
            try
            {
                FileInfo fi = new FileInfo(m_fileInstance.FilePath);
                string fileName = m_fileInstance.FileType;

                if (m_lastUpdatedTime < fi.LastWriteTime)
                {
                    m_filePath.Value = m_fileInstance.FilePath;
                    m_lastUpdatedTime = fi.LastWriteTime;

                    m_rowCount = GetRowCount();

                    if (m_currRec == 0)
                    {
                        // read first record
                        var data = GetRowData(1);
                        UpdatePropertyValues(data);
                    }
                    UpdateRowCount();
                    UpdateReadRecord();
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: CheckForChanges: Error in reading of csv file: {0}", m_fileInstance.FilePath);
            }
        }
        #endregion

        //Added By : Peetambari
        //Added For: Network Stastus check for machine
        #region "CheckForNetworkStatusChanges"
        /// <summary>
        /// Checks File network status and call GetNetworkstatus function every 5 Minutes
        /// </summary>
        public void CheckForNetworkStatusChanges()
        {
            DateTime today = DateTime.Now;
            try
            {
                m_ConnStatus = GetNetworkstatus();
                UpdateConnectionStatus();
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: CheckForChanges: Error in reading of csv file: {0}", m_fileInstance.FilePath);
            }
        }
        #endregion

        #region "OnAfterCreate"
        /// <summary>
        /// OnAfterCreate
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        protected override void OnAfterCreate(ISystemContext context, NodeState node)
        {
            base.OnAfterCreate(context, node);

            CreateChildren();

            MoveFirst.OnSimpleWriteValue = MoveFirstHandler;
            MoveLast.OnSimpleWriteValue = MoveLastHandler;
            Move.OnSimpleWriteValue = MoveHandler;
            MovePrev.OnSimpleWriteValue = MovePrevHandler;
            MoveNext.OnSimpleWriteValue = MoveNextHandler;
        }
        #endregion

        #region "CreateChildren"
        /// <summary>
        /// This Function create headers as property for the file
        /// </summary>
        private void CreateChildren()
        {
            try
            {
                var headers = GetColumnHeaders();
                m_children = new Dictionary<int, PropertyState>();

                foreach (var kvp in headers)
                {
                    // create property state
                    var state = new PropertyState<string>(null);
                    state.Create(m_context, new NodeId(SymbolicName + "#" + kvp.Key, m_namespaceIndex), new QualifiedName(kvp.Key, m_namespaceIndex), null, false);
                    state.AccessLevel = AccessLevels.CurrentRead;
                    state.UserAccessLevel = AccessLevels.CurrentRead;

                    AddChild(state);
                    m_children.Add(kvp.Value, state);
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: CreateChildren: Error in reading headers of csv file: {0}", m_fileInstance.FilePath);
            }
        }
        #endregion

        #region "MoveFirstHandler"
        /// <summary>
        /// Move and read first row of Csv File.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private ServiceResult MoveFirstHandler(ISystemContext context, NodeState node, ref object value)
        {
            if ((bool)value)
            {
                // move to the first record
                var data = GetRowData(1);
                UpdatePropertyValues(data);
            }
            return ServiceResult.Good;
        }
        #endregion

        #region "MoveLastHandler"
        /// <summary>
        /// Move and read last row of Csv File.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ServiceResult MoveLastHandler(ISystemContext context, NodeState node, ref object value)
        {
            if ((bool)value)
            {
                // move to the last record
                var data = GetRowData(m_rowCount);
                UpdatePropertyValues(data);
            }
            return ServiceResult.Good;
        }
        #endregion

        #region "MoveHandler"
        /// <summary>
        /// Move and read perticular row of Csv File.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ServiceResult MoveHandler(ISystemContext context, NodeState node, ref object value)
        {
            Utils.Trace("Started reading the Row");
            int position;

            if (value != null && int.TryParse(value.ToString(), out position))
            {
                // move to the requested position
                var data = GetRowData(position);
                UpdatePropertyValues(data);
                UpdateReadRecord();
            }
            Utils.Trace("Ended reading the Row");
            return ServiceResult.Good;
        }
        #endregion

        #region "MovePrevHandler"
        /// <summary>
        ///  Move and read previous row of Csv File.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ServiceResult MovePrevHandler(ISystemContext context, NodeState node, ref object value)
        {
            if ((bool)value)
            {
                if (m_currRec == 1)
                {
                    return StatusCodes.BadRequestNotAllowed;
                }
                // move to the previous position
                var data = GetRowData(m_currRec - 1);
                UpdatePropertyValues(data);
            }
            return ServiceResult.Good;
        }
        #endregion

        #region "MoveNextHandler"
        /// <summary>
        /// Move and read next row of Csv File.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="node"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ServiceResult MoveNextHandler(ISystemContext context, NodeState node, ref object value)
        {
            if ((bool)value)
            {
                if (m_currRec == m_rowCount)
                {
                    return StatusCodes.BadRequestNotAllowed;
                }
                // move to the next position
                var data = GetRowData(m_currRec + 1);
                UpdatePropertyValues(data);
            }
            return ServiceResult.Good;
        }
        #endregion

        #region "GetColumnHeaders"
        /// <summary>
        /// This Function is used to get  Headers of Csv File.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, int> GetColumnHeaders()
        {
            Dictionary<string, int> headers = new Dictionary<string, int>();

            try
            {
                using (var stream = new FileStream(m_fileInstance.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string line = reader.ReadLine();

                        var names = line.Split(new char[] { ',' });

                        for (int i = 0; i < names.Length; i++)
                        {
                            names[i] = names[i].Trim().Replace(".", string.Empty).Replace(' ', '_').ToLowerInvariant();
                            names[i] = names[i].Replace("%", "percent");
                            names[i] = names[i].Replace("-", "_");
                            names[i] = names[i].Replace("#", "_");

                            if (!string.IsNullOrEmpty(names[i]))
                            {
                                if (headers.ContainsKey(names[i]) == false)
                                {
                                    headers.Add(names[i], i);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: GetColumnHeaders: Error in reading headers of csv file: {0}", m_fileInstance.FilePath);
            }
            return headers;
        }
        #endregion

        #region "GetRowData"
        /// <summary>
        /// This Function gets/reads the particular row data.
        /// </summary>
        /// <param name="rowNumber"></param>
        /// <returns></returns>
        private Dictionary<int, string> GetRowData(int rowNumber)
        {
            Dictionary<int, string> data = new Dictionary<int, string>();
            try
            {
                using (var stream = new FileStream(m_fileInstance.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string line;
                        int rows = -1;

                        while ((line = reader.ReadLine()) != null)
                        {
                            rows += 1;

                            if (rows == rowNumber)
                            {
                                break;
                            }
                        }

                        var colData = line.Split(new char[] { ',' });

                        // Added For - Null values for some data & Each row count is less than header Count.
                        // Added By - Peetambari ------------
                        // If row count(values) Less than Header Count
                        if (colData.Length < m_children.Count)
                        {
                            var dataLeft = m_children.Count - colData.Length;
                            var rowData = colData.Length;

                            // Loop Till the Records
                            for (int i = 0; i < rowData; i++)
                            {
                                if (!string.IsNullOrEmpty(colData[i]))
                                {
                                    data.Add(i, colData[i]);
                                }
                                else
                                {
                                    data.Add(i, " ");
                                }
                            }
                            // Adding Empty Data for missing column (In a Row)
                            var val = rowData + dataLeft;

                            for (int j = rowData; j < val; j++)
                            {
                                data.Add(j, " ");
                            }
                        }
                        else
                        {
                            //Normal loop for correct data
                            foreach (var kvp in m_children)
                            {
                                if (!string.IsNullOrEmpty(colData[kvp.Key]))
                                {
                                    data.Add(kvp.Key, colData[kvp.Key]);
                                }
                                else
                                {
                                    data.Add(kvp.Key, " ");
                                }
                            }
                        }
                        m_currRec = rowNumber;
                    }
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: GetRowData: Error in parsing csv file: {0}", m_fileInstance.FilePath);
            }
            return data;
        }
        #endregion

        #region "GetRowCount"
        /// <summary>
        /// Function to get the Total number of rows in file.
        /// </summary>
        /// <returns></returns>
        private int GetRowCount()
        {
            int rows = -1;

            try
            {
                using (var stream = new FileStream(m_fileInstance.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            rows += 1;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: GetRowCount: Error in parsing csv file: {0}", m_fileInstance.FilePath);
            }
            return rows;
        }
        #endregion

        #region "UpdatePropertyValues"
        /// <summary>
        /// Function used to update row data to property values
        /// </summary>
        /// <param name="data"></param>
        private void UpdatePropertyValues(Dictionary<int, string> data)
        {
            try
            {
                foreach (var kvp in m_children)
                {
                    if (data.ContainsKey(kvp.Key))
                    {
                        kvp.Value.Value = data[kvp.Key];
                    }
                }
                CurrentRecord.Value = m_currRec;
                ClearChangeMasks(m_context, true);
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: UpdatePropertyValues: Error in updating properties");
            }
        }
        #endregion

        #region "UpdateRowCount"
        /// <summary>
        /// update RecordCount property value
        /// </summary>
        private void UpdateRowCount()
        {
            try
            {
                m_lastUpdateTime.Value = m_lastUpdatedTime;
                RecordCount.Value = m_rowCount;
                IsConnected.Value = m_ConnStatus;

                ClearChangeMasks(m_context, true);
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: UpdateRowCount: Error in updating Row Count");
            }
        }
        #endregion

        #region "UpdateReadRecord"
        /// <summary>
        /// update ReadCount property value
        /// </summary>
        private void UpdateReadRecord()
        {
            try
            {
                ReadRecord.Value = m_currRec;
                ClearChangeMasks(m_context, true);
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: UpdateReadRecord: Error in updating Read Record value");
            }
        }
        #endregion

        #region "UpdateConnectionStatus"
        /// <summary>
        ///  Function to update IsConnected property value
        /// </summary>
        private void UpdateConnectionStatus()
        {
            try
            {
                IsConnected.Value = m_ConnStatus;
                ClearChangeMasks(m_context, true);
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: UpdateConnectionStatus: Network Error");
            }
        }
        #endregion

        #region"GetNetworkstatus"
        /// <summary>
        /// For Network Connection check
        /// </summary>
        /// <param name="fi"></param>
        /// <returns></returns>
        private string GetNetworkstatus()
        {
            string conn = "false";

            string url = m_fileInstance.FilePath;
            string fileName = m_fileInstance.FileType;
            Opc.Ua.Utils.Trace(" m_fileInstance fi is " + m_fileInstance.FilePath);
            Opc.Ua.Utils.Trace("DirectoryName is " + url);

            char splitchar = '\\';
            List<string> lstGetIP = url.Split(splitchar).ToList();
            string ipAddress = lstGetIP[2];
            Opc.Ua.Utils.Trace("lstGetIP1 is " + lstGetIP[1]);
            Opc.Ua.Utils.Trace("lstGetIP0 is " + lstGetIP[0]);

            bool pingable = false;
            Ping pinger = null;

            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(ipAddress);
                pingable = reply.Status == IPStatus.Success;
                if (reply.Status == IPStatus.Success)
                {
                    conn = "true";
                }
                Opc.Ua.Utils.Trace("The Network Connection is " + reply.Status);
                if (dict.ContainsKey(fileName))
                {
                    string oldConnStatus = dict[fileName];
                    if (oldConnStatus == "false" && conn == "true")
                    {
                        Opc.Ua.Utils.Trace("Machine Network Reconnected " + m_fileInstance.FilePath);
                        Opc.Ua.Utils.Trace("Machine Path " + url);
                    }
                    dict[fileName] = conn;
                }
                else
                {
                    dict.Add(fileName, conn);
                }
            }
            catch (Exception exception)
            {
                Opc.Ua.Utils.Trace(exception, "GenericCsvState: GetIsConnectedStatus: Network Error for csv file: {0}", m_fileInstance.FilePath);
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }
            Opc.Ua.Utils.Trace("The Network Connection is " + m_fileInstance.FilePath);

            return conn;
        }
        #endregion
    }
}
