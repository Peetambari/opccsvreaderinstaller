﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystem
{
    public interface IMonitoredState
    {
        void CheckForChanges();

       void CheckForNetworkStatusChanges();
    }
}
